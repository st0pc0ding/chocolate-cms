package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsTag;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsTagMapper extends Mapper<TCmsTag> {
}