package com.chocolate.module.web.cms.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.chocolate.mybatis.model.TCmsTag;
import com.chocolate.mybatis.model.TCmsTagGroup;

public interface TagService {

	String save(TCmsTag pojo);
	
	String update(TCmsTag pojo);

    String delete(Integer [] ids);

    List<TCmsTag> tagList();

    PageInfo<TCmsTag> page(Integer pageNumber, Integer pageSize);

    Integer AddOrUpdateTagContent(Long contentId, Integer tagId);
    
    TCmsTagGroup findGroupById(Integer id);
    
    String saveGroup(TCmsTagGroup group);
    
    String updateGroup(TCmsTagGroup group);
    
    String deleteGroupById(Integer id);
    
    List<TCmsTagGroup> findGroupAll();
    
    PageInfo<TCmsTagGroup> page(Integer pageNumber, Integer pageSize, TCmsTagGroup group);
}