package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsTagGroup;

import tk.mybatis.mapper.common.Mapper;

public interface TCmsTagGroupMapper extends Mapper<TCmsTagGroup> {
}