package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsAdGroup;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsAdGroupMapper extends Mapper<TCmsAdGroup> {
}