package com.chocolate.mybatis.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_cms_image")
public class TCmsImage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图片id
     */
    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Integer imageId;

    /**
     * 图片原ID
     */
    @Column(name = "source_id")
    private String sourceId;

    /**
     * 站点编号
     */
    @Column(name = "site_id")
    private Integer siteId;

    /**
     * 图片名称
     */
    @Column(name = "img_name")
    private String imgName;

    /**
     * 图片地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 图片地址-大
     */
    @Column(name = "b_url")
    private String bUrl;
    /**
     * 关键字
     */
    @Column(name = "key_word")
    private String keyWord;

    /**
     * 描述
     */
    @Column(name = "img_introduction")
    private String imgIntroduction;

    /**
     * 来源
     */
    @Column(name = "img_source")
    private String imgSource;

    /**
     * 拍摄时间
     */
    @Column(name = "shooting_time")
    private Date shootingTime;

    /**
     * 上传时间
     */
    @Column(name = "upload_time")
    private Date uploadTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 互联网用户ID
     */
    @Column(name = "web_user_id")
    private String webUserId;
    
    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;
    
    /**
     * 互联网用户名
     */
    @Column(name = "web_user_name")
    private String webUserName;

    /**
     * 图片状态
     * 1:有效 0:个人用户删除 2:wcm删除 3:微信上传
     */
    @Column(name = "img_status")
    private Integer imgStatus;

    /**
     * 图片审核状态
     * 0:未审核;1:送审;2:待审;3:正在审核;4:审核通过;5:审核未通过
     */
    @Column(name = "img_auditing_status")
    private Integer imgAuditingStatus;

    /**
     * 地址
     */
    @Column(name = "address")
    private String address;

    /**
     * 经度
     */
    @Column(name = "longitude")
    private String longitude;

    /**
     * 纬度
     */
    @Column(name = "latitude")
    private String latitude;

    /**
     * 省
     */
    @Column(name = "province")
    private String province;

    /**
     * 市
     */
    @Column(name = "city")
    private String city;

    /**
     * 区
     */
    @Column(name = "district")
    private String district;

    /**
     * 街道
     */
    @Column(name = "street")
    private String street;
    
    /**
     * 查看数
     */
    @Column(name = "view_num")
    private Integer viewNum;
    
    @Column(name = "img_content")
    private String imgContent;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getBUrl() {
		return bUrl;
	}

	public void setBUrl(String bUrl) {
		this.bUrl = bUrl;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getImgIntroduction() {
		return imgIntroduction;
	}

	public void setImgIntroduction(String imgIntroduction) {
		this.imgIntroduction = imgIntroduction;
	}

	public String getImgSource() {
		return imgSource;
	}

	public void setImgSource(String imgSource) {
		this.imgSource = imgSource;
	}

	public Date getShootingTime() {
		return shootingTime;
	}

	public void setShootingTime(Date shootingTime) {
		this.shootingTime = shootingTime;
	}

	public Date getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getWebUserId() {
		return webUserId;
	}

	public void setWebUserId(String webUserId) {
		this.webUserId = webUserId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getWebUserName() {
		return webUserName;
	}

	public void setWebUserName(String webUserName) {
		this.webUserName = webUserName;
	}

	public Integer getImgStatus() {
		return imgStatus;
	}

	public void setImgStatus(Integer imgStatus) {
		this.imgStatus = imgStatus;
	}

	public Integer getImgAuditingStatus() {
		return imgAuditingStatus;
	}

	public void setImgAuditingStatus(Integer imgAuditingStatus) {
		this.imgAuditingStatus = imgAuditingStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getViewNum() {
		return viewNum;
	}

	public void setViewNum(Integer viewNum) {
		this.viewNum = viewNum;
	}

	public String getImgContent() {
		return imgContent;
	}

	public void setImgContent(String imgContent) {
		this.imgContent = imgContent;
	}

}