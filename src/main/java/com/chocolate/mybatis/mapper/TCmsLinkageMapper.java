package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsLinkage;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsLinkageMapper extends Mapper<TCmsLinkage> {
}