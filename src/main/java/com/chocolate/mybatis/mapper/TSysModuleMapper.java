package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TSysModule;
import tk.mybatis.mapper.common.Mapper;

public interface TSysModuleMapper extends Mapper<TSysModule> {
}