package com.chocolate.common.utils;

import com.chocolate.common.constant.CmsConst;
import com.chocolate.module.web.system.vo.UserVo;
import org.apache.shiro.authz.UnauthenticatedException;

/**
 * Description:系统用户工具类
 *
 * 
 * @create 2017-05-31
 **/
public class UserUtil {

    public static UserVo getSysUserVo() {
        UserVo userVo =   ((UserVo) ControllerUtil.getHttpSession().getAttribute(CmsConst.SITE_USER_SESSION_KEY));
        if(CmsUtil.isNullOrEmpty(userVo))
            throw  new UnauthenticatedException();
        return userVo;
    }
}
