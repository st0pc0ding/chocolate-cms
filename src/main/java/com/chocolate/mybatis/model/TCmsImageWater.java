package com.chocolate.mybatis.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_cms_image_water")
public class TCmsImageWater implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 水印id
     */
    @Id
    @Column(name = "water_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Integer waterId;

    /**
     * 站点编号
     */
    @Column(name = "site_id")
    private Integer siteId;

    /**
     * 水印名称
     */
    @Column(name = "water_name")
    private String waterName;

    /**
     * 水印地址
     */
    @Column(name = "water_url")
    private String waterUrl;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "img_content")
    private String imgContent;

	public Integer getWaterId() {
		return waterId;
	}

	public void setWaterId(Integer waterId) {
		this.waterId = waterId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getWaterName() {
		return waterName;
	}

	public void setWaterName(String waterName) {
		this.waterName = waterName;
	}

	public String getWaterUrl() {
		return waterUrl;
	}

	public void setWaterUrl(String waterUrl) {
		this.waterUrl = waterUrl;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getImgContent() {
		return imgContent;
	}

	public void setImgContent(String imgContent) {
		this.imgContent = imgContent;
	}
    
}