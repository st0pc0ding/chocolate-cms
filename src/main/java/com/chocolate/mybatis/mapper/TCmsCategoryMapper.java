package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsCategory;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsCategoryMapper extends Mapper<TCmsCategory> {
}