package com.chocolate.module.web.cms.config;

public class ImageConstants {

	public static final Integer MIDDLE_WIDTH = 658;
	
	public static final Integer MIDDLE_HEIGHT = 658;
	
	public static final Integer SMALL_WIDTH = 260;
	
	public static final Integer SMALL_HEIGHT = 260;
}
