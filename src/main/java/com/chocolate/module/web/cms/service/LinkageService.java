package com.chocolate.module.web.cms.service;

import com.chocolate.common.base.BaseService;
import com.chocolate.mybatis.model.TCmsLinkage;

public interface LinkageService extends BaseService<TCmsLinkage,Integer>{
}
