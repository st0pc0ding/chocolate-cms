package com.chocolate.module.web.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.chocolate.common.utils.JsonUtil;
import com.chocolate.module.web.cms.service.TagService;
import com.chocolate.mybatis.mapper.TCmsTagContentMapper;
import com.chocolate.mybatis.mapper.TCmsTagGroupMapper;
import com.chocolate.mybatis.mapper.TCmsTagMapper;
import com.chocolate.mybatis.model.TCmsTag;
import com.chocolate.mybatis.model.TCmsTagContent;
import com.chocolate.mybatis.model.TCmsTagGroup;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TCmsTagContentMapper tagContentMapper;
    @Autowired
    private TCmsTagMapper tagMapper;
    @Autowired
    private TCmsTagGroupMapper groupMapper;

    @Override
    public String save(TCmsTag pojo) {
        if (tagMapper.insertSelective(pojo)>0)
            return JsonUtil.toSUCCESS("操作成功","tag-tab",true);
        return  JsonUtil.toERROR("操作失败");
    }

    @Override
    public String update(TCmsTag pojo) {
        if (tagMapper.updateByPrimaryKeySelective(pojo)>0)
            return JsonUtil.toSUCCESS("操作成功","tag-tab",true);
        return  JsonUtil.toERROR("操作失败");
    }

    @Override
    public String delete(Integer[] ids) {
        if (ids != null) {
            for (Integer id : ids) {
                tagMapper.deleteByPrimaryKey(id);
                TCmsTagContent ti = new TCmsTagContent();
                ti.setTagId(id);
                tagContentMapper.delete(ti);
            }
        }
        return JsonUtil.toSUCCESS("操作成功", "tag-tab", false);
    }

    @Override
    public List<TCmsTag> tagList() {
        return tagMapper.selectAll();
    }

    @Override
    public PageInfo<TCmsTag> page(Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        return new PageInfo<TCmsTag>(tagList());
    }

    @Override
    public Integer AddOrUpdateTagContent(Long contentId, Integer tagId) {
        TCmsTagContent infoTag = new TCmsTagContent();
        infoTag.setTagId(tagId);
        infoTag.setContentId(Long.valueOf(contentId));
        infoTag = tagContentMapper.selectOne(infoTag);
        if (infoTag == null) {
            infoTag = new TCmsTagContent();
            infoTag.setTagId(tagId);
            infoTag.setContentId(Long.valueOf(contentId));
            return tagContentMapper.insertSelective(infoTag);
        }
        return tagContentMapper.updateByPrimaryKey(infoTag);
    }
    
    @Override
    public String deleteGroupById(Integer id) {
        if(groupMapper.deleteByPrimaryKey(id)>0)
            return JsonUtil.toSUCCESS("操作成功","tag-tab","tag-group-tab",true);
        return  JsonUtil.toERROR("操作失败");
    }

    @Override
    public TCmsTagGroup findGroupById(Integer id) {
        TCmsTagGroup cmsTagGroup = groupMapper.selectByPrimaryKey(id);
        return cmsTagGroup;
    }

    @Override
    public String saveGroup(TCmsTagGroup group) {
        if(groupMapper.insertSelective(group)>0)
             return JsonUtil.toSUCCESS("操作成功","friendlink-tab","tag-group-tab",true);
        return  JsonUtil.toERROR("操作失败");
    }

    @Override
    public String updateGroup(TCmsTagGroup group) {
        if(groupMapper.updateByPrimaryKeySelective(group)>0)
            return JsonUtil.toSUCCESS("操作成功","friendlink-tab","tag-group-tab",true);
        return  JsonUtil.toERROR("操作失败");
    }

    @Override
    public List<TCmsTagGroup> findGroupAll() {
        return groupMapper.selectAll();
    }
    
    @Override
    public PageInfo<TCmsTagGroup> page(Integer pageNumber, Integer pageSize, TCmsTagGroup group) {
        PageHelper.startPage(pageNumber,pageSize);
        return new PageInfo<TCmsTagGroup>(groupMapper.select(group));
    }

}
