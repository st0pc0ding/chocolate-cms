package com.chocolate.module.web.system.service;

import com.github.pagehelper.PageInfo;
import com.chocolate.mybatis.model.TSysAttachment;

import java.util.List;

/**
 * Description:
 *
 * 
 * @create 2017-08-08
 **/
public interface AttachmentService{

    void save(TSysAttachment pojo);

    String delete(Long[] ids);

    TSysAttachment findByKey(String key);

    List<TSysAttachment> findList(TSysAttachment pojo);

    List<TSysAttachment> findAll();

    PageInfo<TSysAttachment> page(Integer pageNumber, Integer pageSize, TSysAttachment pojo);

    PageInfo<TSysAttachment> page(Integer pageNumber, Integer pageSize);

}
