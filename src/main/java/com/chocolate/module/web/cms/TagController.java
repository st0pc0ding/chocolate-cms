package com.chocolate.module.web.cms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chocolate.module.web.cms.service.TagService;
import com.chocolate.mybatis.model.TCmsTag;
import com.chocolate.mybatis.model.TCmsTagGroup;

/**
 * Description:tag
 **/
@Controller
@RequestMapping("/system/cms/tag")
public class TagController{

    @Autowired
    private TagService tagService;

    @RequestMapping
    public String index(@RequestParam(value = "pageCurrent",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "100") Integer pageSize, Model model) {
        model.addAttribute("model", tagService.page(pageNumber,pageSize));
        model.addAttribute("groups",tagService.findGroupAll());
        return "cms/tag_list";
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(TCmsTag pojo) {
        if(pojo.getTagId()!=null)
            return tagService.update(pojo);
        return tagService.save(pojo);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam("ids") Integer[] ids) {
        return tagService.delete(ids);
    }

    @RequestMapping("/group/delete")
    @ResponseBody
    public String groupDelete(@RequestParam(value = "ids",required = false) Integer id) {
        return tagService.deleteGroupById(id);
    }

    @RequestMapping("/group/input")
    public String groupInput(@RequestParam(value = "id",required = false) Integer id,Model model){
        if(id!=null)
            model.addAttribute("group",tagService.findGroupById(id));
        return "cms/tag_group_input";
    }

    @RequestMapping("/group/save")
    @ResponseBody
    public String groupSave(TCmsTagGroup group){
        if(group.getGroupId()!=null)
            return tagService.updateGroup(group);
        return tagService.saveGroup(group);
    }

    @RequestMapping("/group/list")
    public String groupList(@RequestParam(value = "pageCurrent",defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize",defaultValue = "50") Integer pageSize,
                            TCmsTagGroup group,
                            Model model
                            ){
        model.addAttribute("model",tagService.page(pageNumber,pageSize,group));
        model.addAttribute("pojo",group);
        return "cms/tag_group_list";
    }
}
