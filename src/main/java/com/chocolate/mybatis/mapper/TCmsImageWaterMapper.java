package com.chocolate.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import com.chocolate.mybatis.model.TCmsImageWater;

import tk.mybatis.mapper.common.Mapper;

public interface TCmsImageWaterMapper extends Mapper<TCmsImageWater> {

    /*根据站点编号删除内容*/
    @Delete("delete from t_cms_image_water where site_id = #{siteId}")
    int deleteBySiteId(@Param("siteId") Integer siteId);

    List<TCmsImageWater> selectByCondition(TCmsImageWater imageWater);

}