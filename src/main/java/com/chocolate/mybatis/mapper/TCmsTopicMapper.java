package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsTopic;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsTopicMapper extends Mapper<TCmsTopic> {
}