package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TSysOrg;
import tk.mybatis.mapper.common.Mapper;

public interface TSysOrgMapper extends Mapper<TSysOrg> {
}