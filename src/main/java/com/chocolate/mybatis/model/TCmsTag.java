package com.chocolate.mybatis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_cms_tag")
public class TCmsTag implements Serializable {

    private static final long serialVersionUID = 1L;
	
    @Id
    @Column(name = "tag_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Integer tagId;

    @Column(name = "tag_name")
    private String tagName;

    /**
     * 分类ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * @return id
     */
    public Integer getTagId() {
        return tagId;
    }

    /**
     * @param id
     */
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    /**
     * @return tag_name
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * @param tagName
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

}