/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : chocolate_cms

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-11-01 00:00:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_cms_ad`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_ad`;
CREATE TABLE `t_cms_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_name` varchar(150) DEFAULT NULL COMMENT '广告名称',
  `start_date` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `ad_body` text COMMENT '广告内容（如代码）',
  `sort_id` int(11) DEFAULT NULL COMMENT '排序编号',
  `group_id` int(11) DEFAULT NULL COMMENT '广告分组',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='广告';

-- ----------------------------
-- Records of t_cms_ad
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_ad_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_ad_group`;
CREATE TABLE `t_cms_ad_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(150) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='广告分组';

-- ----------------------------
-- Records of t_cms_ad_group
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_ad_silder`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_ad_silder`;
CREATE TABLE `t_cms_ad_silder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `silde_name` varchar(100) NOT NULL DEFAULT '',
  `img` varchar(255) DEFAULT '',
  `silde_url` varchar(255) NOT NULL DEFAULT '',
  `sort_id` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `type` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='幻灯片广告';

-- ----------------------------
-- Records of t_cms_ad_silder
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_category`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_category`;
CREATE TABLE `t_cms_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL DEFAULT '' COMMENT '别名',
  `category_name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类明细',
  `site_id` int(11) DEFAULT '1' COMMENT '站点编号',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父类编号',
  `model_id` int(11) DEFAULT NULL COMMENT '模型编号',
  `alone` tinyint(1) DEFAULT '0' COMMENT '单页（0：不是，1：是）',
  `content` mediumtext COMMENT '文本',
  `index_tpl` varchar(255) NOT NULL DEFAULT '' COMMENT '模板',
  `list_tpl` varchar(255) NOT NULL DEFAULT '' COMMENT '列表页面',
  `content_tpl` varchar(255) NOT NULL DEFAULT '' COMMENT '内容页面',
  `is_nav` tinyint(1) NOT NULL DEFAULT '0' COMMENT '导航',
  `url` varchar(255) DEFAULT NULL COMMENT '地址',
  `has_child` tinyint(1) DEFAULT '0' COMMENT '是否有子类',
  `is_common` tinyint(1) DEFAULT '0' COMMENT '是否为通用栏目（子站点也默认继承此栏目）',
  `allow_front_submit` tinyint(1) DEFAULT '1',
  `page_size` int(11) DEFAULT NULL COMMENT '栏目分页数量',
  `allow_search` tinyint(1) unsigned DEFAULT '0' COMMENT '当前栏目下的是否支持全文搜索',
  `category_icon` varchar(255) DEFAULT NULL COMMENT '栏目图标',
  `permission_key` varchar(100) DEFAULT NULL COMMENT '栏目权限标识',
  `sort_id` int(11) unsigned DEFAULT '0',
  `translated_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `category_id` (`category_id`),
  KEY `short_name` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='分类表';

-- ----------------------------
-- Records of t_cms_category
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_content`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_content`;
CREATE TABLE `t_cms_content` (
  `content_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点编号',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '栏目',
  `model_id` int(11) DEFAULT '0' COMMENT '模型id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` char(40) DEFAULT '' COMMENT '关键字',
  `description` text COMMENT '描述',
  `top` tinyint(1) DEFAULT '0' COMMENT '热门',
  `recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐',
  `thumb` varchar(255) DEFAULT NULL COMMENT '封面',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `inputdate` timestamp NULL DEFAULT NULL COMMENT '发布时间',
  `updatedate` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `url` char(100) DEFAULT '' COMMENT '文章地址',
  `author` char(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `view_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '查看次数',
  PRIMARY KEY (`content_id`),
  KEY `catid` (`category_id`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='内容表';

-- ----------------------------
-- Records of t_cms_content
-- ---------------------------

-- ----------------------------
-- Table structure for `t_cms_content_news`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_content_news`;
CREATE TABLE `t_cms_content_news` (
  `content_id` bigint(20) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `fujian` varchar(128) DEFAULT NULL,
  `laiyuan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_cms_content_news
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_ebook`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_ebook`;
CREATE TABLE `t_cms_ebook` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ebook_name` varchar(200) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `file_path` varchar(2000) DEFAULT NULL,
  `view_name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_cms_ebook
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_form`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_form`;
CREATE TABLE `t_cms_form` (
  `form_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `from_name` varchar(255) DEFAULT NULL COMMENT '表单名称',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `from_action` varchar(255) DEFAULT NULL COMMENT '表单提交地址',
  `required_login` tinyint(1) unsigned DEFAULT NULL COMMENT '是否登陆',
  `return_type` int(1) DEFAULT NULL COMMENT '返回类型（1 JSON 、2 html）',
  `enable_verify_code` tinyint(1) DEFAULT NULL COMMENT '开启验证码',
  PRIMARY KEY (`form_id`),
  KEY `from_action` (`from_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='表单';

-- ----------------------------
-- Records of t_cms_form
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_friendlink`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_friendlink`;
CREATE TABLE `t_cms_friendlink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_name` varchar(100) NOT NULL DEFAULT '',
  `link_type` int(1) NOT NULL DEFAULT '0',
  `img` varchar(255) DEFAULT '',
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `sort_id` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `group_id` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='友情链接';

-- ----------------------------
-- Records of t_cms_friendlink
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_friendlink_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_friendlink_group`;
CREATE TABLE `t_cms_friendlink_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_cms_friendlink_group
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_linkage`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_linkage`;
CREATE TABLE `t_cms_linkage` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点id',
  `name` varchar(30) NOT NULL,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `grade` int(1) NOT NULL DEFAULT '0',
  `childs` varchar(200) NOT NULL DEFAULT '',
  `sort_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_common` tinyint(1) DEFAULT '0' COMMENT '是否为通用',
  PRIMARY KEY (`id`),
  KEY `child` (`grade`),
  KEY `keyid` (`site_id`),
  KEY `list` (`site_id`,`parent_id`,`sort_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_cms_linkage
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_model`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_model`;
CREATE TABLE `t_cms_model` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) DEFAULT NULL COMMENT '模型名称',
  `table_name` varchar(255) DEFAULT NULL COMMENT '模型表名称',
  `site_id` bigint(20) DEFAULT NULL COMMENT '站点id',
  `des` varchar(255) DEFAULT '' COMMENT '字段描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='模型表';

-- ----------------------------
-- Records of t_cms_model
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_model_filed`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_model_filed`;
CREATE TABLE `t_cms_model_filed` (
  `filed_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字段编号',
  `model_id` int(11) NOT NULL DEFAULT '0' COMMENT '模型编号',
  `filed_name` varchar(255) NOT NULL DEFAULT '' COMMENT '字段名称',
  `filed_class` varchar(255) NOT NULL DEFAULT '' COMMENT '字段类型（如文本）',
  `filed_value` text COMMENT '字段值',
  `filed_type` varchar(255) NOT NULL DEFAULT '0' COMMENT '字段类别（数据库类别）',
  `alias` varchar(255) NOT NULL DEFAULT '' COMMENT '别名',
  `not_null` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为空',
  `filed_length` int(11) DEFAULT '0' COMMENT '字段长度',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为主键',
  `setting` varchar(255) DEFAULT '',
  PRIMARY KEY (`filed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='模型字段';

-- ----------------------------
-- Records of t_cms_model_filed
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_search_words`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_search_words`;
CREATE TABLE `t_cms_search_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '搜索词汇',
  `hit_count` int(11) NOT NULL DEFAULT '0' COMMENT '搜索次数',
  `is_recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐',
  `site_id` int(11) NOT NULL DEFAULT '1' COMMENT '站点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='搜索热词';

-- ----------------------------
-- Records of t_cms_search_words
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_site`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_site`;
CREATE TABLE `t_cms_site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '站点id（0为主站，不能删除）',
  `site_name` varchar(255) DEFAULT NULL COMMENT '站点名称',
  `domain` varchar(255) DEFAULT '' COMMENT '域名',
  `language` char(10) DEFAULT '' COMMENT '语言',
  `template` varchar(20) DEFAULT '' COMMENT '模板',
  `title` varchar(64) DEFAULT NULL,
  `keyword` varchar(60) DEFAULT '' COMMENT '关键字',
  `description` text COMMENT '描述',
  `is_mobile` tinyint(1) DEFAULT '0' COMMENT '移动是否开启',
  `mobile_tpl` varchar(255) DEFAULT 'mobile' COMMENT '手机模板',
  `status` tinyint(1) DEFAULT '0' COMMENT '站点状态',
  `logo` varchar(255) DEFAULT NULL,
  `site_key` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='站点';

-- ----------------------------
-- Records of t_cms_site
-- ----------------------------
INSERT INTO `t_cms_site` VALUES ('1', 'test', 'localhost', '', 'test', 'test', 'test', 'test', '1', 'moble', '1', '', 'test');

-- ----------------------------
-- Table structure for `t_cms_tag`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_tag`;
CREATE TABLE `t_cms_tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(64) NOT NULL DEFAULT '',
  `group_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tag_id`),
  KEY `name` (`tag_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='cmstag';

-- ----------------------------
-- Records of t_cms_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_tag_group`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_tag_group`;
CREATE TABLE `t_cms_tag_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`group_id`),
  KEY `name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='cmsgroup';

-- ----------------------------
-- Records of t_cms_tag_group
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_tag_content`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_tag_content`;
CREATE TABLE `t_cms_tag_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `content_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_id` (`content_id`),
  KEY `tag_Id` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='标签内容';

-- ----------------------------
-- Records of t_cms_tag_content
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_topic`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_topic`;
CREATE TABLE `t_cms_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_ids` varchar(2000) DEFAULT NULL,
  `topic_name` varchar(150) NOT NULL COMMENT '名称',
  `short_name` varchar(150) DEFAULT NULL COMMENT '简称',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `title_img` varchar(100) DEFAULT NULL COMMENT '标题图',
  `content_img` varchar(100) DEFAULT NULL COMMENT '内容图',
  `topic_tpl` varchar(100) DEFAULT NULL COMMENT '专题模板',
  `sort_id` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `is_recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推??',
  `site_id` int(11) DEFAULT NULL COMMENT '站点Id',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='CMS专题表';

-- ----------------------------
-- Records of t_cms_topic
-- ----------------------------

-- ----------------------------
-- Table structure for `t_cms_user_site`
-- ----------------------------
DROP TABLE IF EXISTS `t_cms_user_site`;
CREATE TABLE `t_cms_user_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id（这个用户id只代表后台管理员用户）',
  `site_id` int(11) DEFAULT NULL COMMENT '站点id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统管理员和用户关联表';

-- ----------------------------
-- Records of t_cms_user_site
-- ----------------------------
INSERT INTO `t_cms_user_site` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `t_sys_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_attachment`;
CREATE TABLE `t_sys_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) NOT NULL COMMENT '文件名称',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `username` varchar(32) DEFAULT NULL,
  `upload_date` datetime NOT NULL COMMENT '上传时间',
  `upload_ip` varchar(100) NOT NULL DEFAULT '' COMMENT '上传的ID',
  `file_extname` varchar(100) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `file_path` varchar(500) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '文件大小',
  `url` varchar(500) DEFAULT NULL COMMENT '附件网址',
  `file_key` varchar(255) DEFAULT NULL COMMENT '文件key',
  `original_file_name` varchar(255) DEFAULT NULL COMMENT '源文件名称',
  `type` int(11) DEFAULT '1' COMMENT '（1,附件，2电子书）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='附件表';

-- ----------------------------
-- Records of t_sys_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log`;
CREATE TABLE `t_sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `createTime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=utf8mb4 COMMENT='日志表';

-- ----------------------------
-- Records of t_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_module`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_module`;
CREATE TABLE `t_sys_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `is_enable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用',
  `is_auto_expand` tinyint(1) DEFAULT NULL COMMENT '是否自动展开',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单url',
  `icon_name` varchar(64) DEFAULT NULL COMMENT '菜单节点图标',
  `permission_key` varchar(64) DEFAULT NULL COMMENT '权限key',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父id编号',
  `has_child` tinyint(1) NOT NULL DEFAULT '0' COMMENT '有子节点',
  `sort_no` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_sys_module
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_org`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_org`;
CREATE TABLE `t_sys_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '组织名称',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `code` varchar(64) NOT NULL COMMENT '编号',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `tel_phone` varchar(12) DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='组织机构表';

-- ----------------------------
-- Records of t_sys_org
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_org_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_org_user`;
CREATE TABLE `t_sys_org_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '机构编号',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色编号',
  PRIMARY KEY (`id`),
  KEY `org_id` (`org_id`),
  KEY `role_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='机构权限表';

-- ----------------------------
-- Records of t_sys_org_user
-- ----------------------------


-- ----------------------------
-- Table structure for `t_sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_permission`;
CREATE TABLE `t_sys_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COMMENT='权限';

-- ----------------------------
-- Records of t_sys_permission
-- ----------------------------
INSERT INTO `t_sys_permission` VALUES ('1', 'site:admin', '站点管理', '7');
INSERT INTO `t_sys_permission` VALUES ('7', 'neirongmokuai', '内容模块', '48');
INSERT INTO `t_sys_permission` VALUES ('10', 'content:admin', '内容管理', '7');
INSERT INTO `t_sys_permission` VALUES ('12', 'yonghumokuai', '用户模块', '48');
INSERT INTO `t_sys_permission` VALUES ('13', 'sysUser:admin', '系统用户', '12');
INSERT INTO `t_sys_permission` VALUES ('14', 'category:admin', '栏目管理', '7');
INSERT INTO `t_sys_permission` VALUES ('15', 'model:admin', '模型管理', '7');
INSERT INTO `t_sys_permission` VALUES ('16', 'role:admin', '角色管理', '12');
INSERT INTO `t_sys_permission` VALUES ('17', 'permission:admin', '权限管理', '12');
INSERT INTO `t_sys_permission` VALUES ('19', 'site:delete', '站点删除', '1');
INSERT INTO `t_sys_permission` VALUES ('20', 'site:save', '站点保存', '1');
INSERT INTO `t_sys_permission` VALUES ('21', 'content:input', '内容添加', '10');
INSERT INTO `t_sys_permission` VALUES ('22', 'content:delete', '内容删除', '10');
INSERT INTO `t_sys_permission` VALUES ('23', 'category:input', '栏目添加', '14');
INSERT INTO `t_sys_permission` VALUES ('24', 'category:delete', '栏目删除', '14');
INSERT INTO `t_sys_permission` VALUES ('25', 'model:input', '模型添加', '15');
INSERT INTO `t_sys_permission` VALUES ('26', 'model:delete', '模型删除', '15');
INSERT INTO `t_sys_permission` VALUES ('27', 'modelFiled:input', '模型字段添加', '15');
INSERT INTO `t_sys_permission` VALUES ('28', 'modelFiled:delete', '模型字段删除', '15');
INSERT INTO `t_sys_permission` VALUES ('29', 'site:change', '站点切换', '1');
INSERT INTO `t_sys_permission` VALUES ('30', 'content:save', '内容保存', '10');
INSERT INTO `t_sys_permission` VALUES ('31', 'model:save', '模型保存', '15');
INSERT INTO `t_sys_permission` VALUES ('32', 'modelFiled:save', '模型字段保存', '15');
INSERT INTO `t_sys_permission` VALUES ('33', 'category:save', '栏目保存', '14');
INSERT INTO `t_sys_permission` VALUES ('34', 'site:input', '站点添加', '1');
INSERT INTO `t_sys_permission` VALUES ('35', 'sysUser:input', '用户添加', '13');
INSERT INTO `t_sys_permission` VALUES ('36', 'sysUser:delete', '用户删除', '13');
INSERT INTO `t_sys_permission` VALUES ('37', 'sysUser:save', '用户保存', '13');
INSERT INTO `t_sys_permission` VALUES ('38', 'role:input', '角色添加', '16');
INSERT INTO `t_sys_permission` VALUES ('39', 'role:save', '角色保存', '16');
INSERT INTO `t_sys_permission` VALUES ('40', 'role:delete', '角色删除', '16');
INSERT INTO `t_sys_permission` VALUES ('41', 'permission:input', '权限添加', '17');
INSERT INTO `t_sys_permission` VALUES ('42', 'permission:save', '权限保存', '17');
INSERT INTO `t_sys_permission` VALUES ('43', 'permission:delete', '权限删除', '17');
INSERT INTO `t_sys_permission` VALUES ('44', 'xitongxiangguan', '系统相关', '48');
INSERT INTO `t_sys_permission` VALUES ('45', 'log:admin', '日志管理', '44');
INSERT INTO `t_sys_permission` VALUES ('46', 'log:view', '日志浏览', '45');
INSERT INTO `t_sys_permission` VALUES ('47', 'log:delete', '日志删除', '45');
INSERT INTO `t_sys_permission` VALUES ('48', 'system', '系统权限', '0');
INSERT INTO `t_sys_permission` VALUES ('49', 'ad:admin', '广告管理', '48');
INSERT INTO `t_sys_permission` VALUES ('50', 'friendlink:admin', '友情链接', '48');
INSERT INTO `t_sys_permission` VALUES ('51', 'indexsilder:admin', '首页幻灯', '48');
INSERT INTO `t_sys_permission` VALUES ('52', 'template:admin', '模板管理', '48');
INSERT INTO `t_sys_permission` VALUES ('53', 'template:edit', '模板编辑', '52');
INSERT INTO `t_sys_permission` VALUES ('54', 'template:save', '模板保存', '52');
INSERT INTO `t_sys_permission` VALUES ('55', 'department:admin', '部门管理', '48');
INSERT INTO `t_sys_permission` VALUES ('56', 'department:save', '部门保存', '55');
INSERT INTO `t_sys_permission` VALUES ('57', 'department:delete', '部门删除', '55');
INSERT INTO `t_sys_permission` VALUES ('58', 'job:admin', '定时任务', '48');
INSERT INTO `t_sys_permission` VALUES ('59', 'job:input', '任务添加', '58');
INSERT INTO `t_sys_permission` VALUES ('60', 'job:save', '任务保存 ', '58');
INSERT INTO `t_sys_permission` VALUES ('61', 'job:delete', '任务删除', '58');

-- ----------------------------
-- Table structure for `t_sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `rolename` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` text COMMENT '描述',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES ('1', 'SuperAdmin', '超级管理员');

-- ----------------------------
-- Table structure for `t_sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_permission`;
CREATE TABLE `t_sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `permisson_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pid` (`permisson_id`),
  KEY `rid` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=738 DEFAULT CHARSET=utf8mb4 COMMENT='角色权限表';

-- ----------------------------
-- Records of t_sys_role_permission
-- ----------------------------
INSERT INTO `t_sys_role_permission` VALUES ('686', '1', '48');
INSERT INTO `t_sys_role_permission` VALUES ('687', '1', '7');
INSERT INTO `t_sys_role_permission` VALUES ('688', '1', '1');
INSERT INTO `t_sys_role_permission` VALUES ('689', '1', '19');
INSERT INTO `t_sys_role_permission` VALUES ('690', '1', '20');
INSERT INTO `t_sys_role_permission` VALUES ('691', '1', '29');
INSERT INTO `t_sys_role_permission` VALUES ('692', '1', '34');
INSERT INTO `t_sys_role_permission` VALUES ('693', '1', '10');
INSERT INTO `t_sys_role_permission` VALUES ('694', '1', '21');
INSERT INTO `t_sys_role_permission` VALUES ('695', '1', '22');
INSERT INTO `t_sys_role_permission` VALUES ('696', '1', '30');
INSERT INTO `t_sys_role_permission` VALUES ('697', '1', '14');
INSERT INTO `t_sys_role_permission` VALUES ('698', '1', '23');
INSERT INTO `t_sys_role_permission` VALUES ('699', '1', '24');
INSERT INTO `t_sys_role_permission` VALUES ('700', '1', '33');
INSERT INTO `t_sys_role_permission` VALUES ('701', '1', '15');
INSERT INTO `t_sys_role_permission` VALUES ('702', '1', '25');
INSERT INTO `t_sys_role_permission` VALUES ('703', '1', '26');
INSERT INTO `t_sys_role_permission` VALUES ('704', '1', '27');
INSERT INTO `t_sys_role_permission` VALUES ('705', '1', '28');
INSERT INTO `t_sys_role_permission` VALUES ('706', '1', '31');
INSERT INTO `t_sys_role_permission` VALUES ('707', '1', '32');
INSERT INTO `t_sys_role_permission` VALUES ('708', '1', '12');
INSERT INTO `t_sys_role_permission` VALUES ('709', '1', '13');
INSERT INTO `t_sys_role_permission` VALUES ('710', '1', '35');
INSERT INTO `t_sys_role_permission` VALUES ('711', '1', '36');
INSERT INTO `t_sys_role_permission` VALUES ('712', '1', '37');
INSERT INTO `t_sys_role_permission` VALUES ('713', '1', '16');
INSERT INTO `t_sys_role_permission` VALUES ('714', '1', '38');
INSERT INTO `t_sys_role_permission` VALUES ('715', '1', '39');
INSERT INTO `t_sys_role_permission` VALUES ('716', '1', '40');
INSERT INTO `t_sys_role_permission` VALUES ('717', '1', '17');
INSERT INTO `t_sys_role_permission` VALUES ('718', '1', '41');
INSERT INTO `t_sys_role_permission` VALUES ('719', '1', '42');
INSERT INTO `t_sys_role_permission` VALUES ('720', '1', '43');
INSERT INTO `t_sys_role_permission` VALUES ('721', '1', '44');
INSERT INTO `t_sys_role_permission` VALUES ('722', '1', '45');
INSERT INTO `t_sys_role_permission` VALUES ('723', '1', '46');
INSERT INTO `t_sys_role_permission` VALUES ('724', '1', '47');
INSERT INTO `t_sys_role_permission` VALUES ('725', '1', '49');
INSERT INTO `t_sys_role_permission` VALUES ('726', '1', '50');
INSERT INTO `t_sys_role_permission` VALUES ('727', '1', '51');
INSERT INTO `t_sys_role_permission` VALUES ('728', '1', '52');
INSERT INTO `t_sys_role_permission` VALUES ('729', '1', '53');
INSERT INTO `t_sys_role_permission` VALUES ('730', '1', '54');
INSERT INTO `t_sys_role_permission` VALUES ('731', '1', '55');
INSERT INTO `t_sys_role_permission` VALUES ('732', '1', '56');
INSERT INTO `t_sys_role_permission` VALUES ('733', '1', '57');
INSERT INTO `t_sys_role_permission` VALUES ('734', '1', '58');
INSERT INTO `t_sys_role_permission` VALUES ('735', '1', '59');
INSERT INTO `t_sys_role_permission` VALUES ('736', '1', '60');
INSERT INTO `t_sys_role_permission` VALUES ('737', '1', '61');

-- ----------------------------
-- Table structure for `t_sys_schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_schedule_job`;
CREATE TABLE `t_sys_schedule_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_expression` varchar(255) NOT NULL COMMENT 'cron表达式',
  `method_name` varchar(255) NOT NULL COMMENT '任务调用的方法名',
  `is_concurrent` varchar(255) DEFAULT NULL COMMENT '任务是否有状态',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `bean_class` varchar(255) DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `job_status` varchar(255) DEFAULT NULL COMMENT '任务状态',
  `job_group` varchar(255) DEFAULT NULL COMMENT '任务分组',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `spring_bean` varchar(255) DEFAULT NULL COMMENT 'Spring bean',
  `job_name` varchar(255) DEFAULT NULL COMMENT '任务名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='任务调度';

-- ----------------------------
-- Records of t_sys_schedule_job
-- ----------------------------

-- ----------------------------
-- Table structure for `t_sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id（0为创始人不能删除）',
  `username` varchar(64) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) NOT NULL DEFAULT '' COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '用户创建时间',
  `salt` varchar(64) DEFAULT NULL COMMENT '用户盐',
  `login_time` timestamp NULL DEFAULT NULL COMMENT '登陆时间',
  `logout_time` timestamp NULL DEFAULT NULL COMMENT '离开时间',
  `last_ip` varchar(100) DEFAULT '' COMMENT '登陆IP',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '管理员状态',
  `des` varchar(255) DEFAULT '' COMMENT '管理员说明',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES ('1', 'admin', '3266bb93948cd26fd302dd2d58a478af381d75879ed9f3dcf309470c50f88fd6', '2020-10-27 21:08:29', '21232f297a57a5a743894a0e4a801fc3', '2020-11-01 00:00:00', null, '127.0.0.1', '1', '超级管理', '');

-- ----------------------------
-- Table structure for `t_sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_role`;
CREATE TABLE `t_sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='管理员权限';

-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO `t_sys_user_role` VALUES ('1', '1', '1');
