package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TSysScheduleJob;
import tk.mybatis.mapper.common.Mapper;

public interface TSysScheduleJobMapper extends Mapper<TSysScheduleJob> {
}