package com.chocolate.module.web.cms.service;

import com.chocolate.common.base.BaseService;
import com.chocolate.mybatis.model.TCmsTopic;

import java.util.List;

public interface TopicService extends BaseService<TCmsTopic,Integer> {
    Integer AllCount();

    List<TCmsTopic> findByRecommendList(Integer siteId,boolean isRecommend, Integer pageSize);
}
