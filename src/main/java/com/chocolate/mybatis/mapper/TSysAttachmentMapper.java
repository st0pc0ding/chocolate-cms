package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TSysAttachment;
import tk.mybatis.mapper.common.Mapper;

public interface TSysAttachmentMapper extends Mapper<TSysAttachment> {
}