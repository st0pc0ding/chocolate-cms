package com.chocolate.module.web.cms.service;


import com.chocolate.common.base.BaseService;
import com.chocolate.module.web.cms.vo.TCmsSiteVo;
import com.chocolate.module.web.system.vo.UserVo;
import com.chocolate.mybatis.model.TCmsSite;

/**
 * Created by binary on 2017/5/15.
 */
public interface SiteService extends BaseService<TCmsSite,Integer> {

    String save(TCmsSite pojo);

    String update(TCmsSite pojo);

    String save(TCmsSiteVo pojo);

    String update(TCmsSiteVo pojo);

    TCmsSiteVo findVoById(Integer id);

    TCmsSite findBySiteKey(String key);

    String change(UserVo userVo, Integer siteId);

    TCmsSite findByDomain(String domain);
}
