package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsModel;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsModelMapper extends Mapper<TCmsModel> {
}