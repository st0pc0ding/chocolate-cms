package com.chocolate.module.web.cms.service;

import com.chocolate.common.base.BaseService;
import com.chocolate.mybatis.model.TCmsAdSilder;

/**
 * Description:幻灯片
 *
 * 
 * @create 2017-06-12
 **/
public interface SilderService extends BaseService<TCmsAdSilder,Integer>{
}
