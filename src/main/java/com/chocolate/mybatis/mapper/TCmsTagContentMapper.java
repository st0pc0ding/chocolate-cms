package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsTagContent;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsTagContentMapper extends Mapper<TCmsTagContent> {
}