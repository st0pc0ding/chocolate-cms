package com.chocolate.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.chocolate.mybatis.model.TCmsImage;

import tk.mybatis.mapper.common.Mapper;

public interface TCmsImageMapper extends Mapper<TCmsImage> {

    /*根据站点编号删除内容*/
    @Delete("delete from t_cms_image where site_id = #{siteId}")
    int deleteBySiteId(@Param("siteId") Integer siteId);

    @Select("select * from t_cms_image where image_id = #{imageId}")
    @ResultMap("BaseResultMap")
    TCmsImage selectById(@Param("imageId") Long imageId);

    List<TCmsImage> selectByCondition(TCmsImage image);

}