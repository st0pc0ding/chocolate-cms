package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsAdSilder;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsAdSilderMapper extends Mapper<TCmsAdSilder> {
}