package com.chocolate.mybatis.mapper;

import com.chocolate.mybatis.model.TCmsFriendlinkGroup;
import tk.mybatis.mapper.common.Mapper;

public interface TCmsFriendlinkGroupMapper extends Mapper<TCmsFriendlinkGroup> {
}